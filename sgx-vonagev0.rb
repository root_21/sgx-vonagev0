#!/usr/bin/env ruby
#
# Copyright (C) 2017-2020  Denver Gingerich <denver@ossguy.com>
# Copyright (C) 2017  Stephen Paul Weber <singpolyma@singpolyma.net>
#
# This file is part of sgx-vonagev0.
#
# sgx-vonagev0 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# sgx-vonagev0 is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with sgx-vonagev0.  If not, see <http://www.gnu.org/licenses/>.

require 'blather/client/dsl'
require 'em-hiredis'
require 'em-http-request'
require 'json'
require 'securerandom'
require 'time'
require 'uri'
require 'webrick'

require 'goliath/api'
require 'goliath/server'
require 'log4r'

require_relative 'em_promise'

def panic(e)
	puts "Shutting down gateway due to exception: #{e.message}"
	puts e.backtrace
	SGXvonagev0.shutdown
	puts 'Gateway has terminated.'
	EM.stop
end

def extract_shortcode(dest)
	num, context = dest.split(';', 2)
	num if context && context == 'phone-context=ca-us.phone-context.soprani.ca'
end

def is_anonymous_tel?(dest)
	num, context = dest.split(';', 2)
	context && context == 'phone-context=anonymous.phone-context.soprani.ca'
end

def escape_jid(localpart)
	# TODO: proper XEP-0106 Sec 4.3, ie. pre-escaped
	localpart
		.gsub("\\", "\\\\5c")
		.gsub(' ', "\\\\20")
		.gsub('"', "\\\\22")
		.gsub('&', "\\\\26")
		.gsub("'", "\\\\27")
		.gsub('/', "\\\\2f")
		.gsub(':', "\\\\3a")
		.gsub('<', "\\\\3c")
		.gsub('>', "\\\\3e")
		.gsub('@', "\\\\40")
end

class SGXClient < Blather::Client
	def register_handler(type, *guards, &block)
		super(type, *guards) { |*args| wrap_handler(*args, &block) }
	end

	def register_handler_before(type, *guards, &block)
		check_handler(type, guards)
		handler = lambda { |*args| wrap_handler(*args, &block) }

		@handlers[type] ||= []
		@handlers[type].unshift([guards, handler])
	end

protected

	def wrap_handler(*args, &block)
		v = block.call(*args)
		v.catch(&method(:panic)) if v.is_a?(Promise)
		true # Do not run other handlers unless throw :pass
	rescue Exception => e
		panic(e)
	end
end

module SGXvonagev0
	extend Blather::DSL

	@last_message_text = Hash.new('')
	@last_message_count = Hash.new(1)
	@last_message_had_httpx = Hash.new(false)
	@last_messages_with_httpx_count = Hash.new(1)
	@client = SGXClient.new
	@gateway_features = [
		"http://jabber.org/protocol/disco#info",
		"jabber:iq:register"
	]

	def self.run
		client.run
	end

	# so classes outside this module can write messages, too
	def self.write(stanza)
		client.write(stanza)
	end

	def self.before_handler(type, *guards, &block)
		client.register_handler_before(type, *guards, &block)
	end

	def self.send_media(from, to, media_url, desc=nil, subject=nil)
		# we assume media_url is of the form (always the case so far):
		#  https://api.catapult.inetwork.com/v1/users/[uid]/media/[file]

		# TODO: need to make ARGV[6] work, or use something else
		# the caller must guarantee that 'to' is a bare JID
		#proxy_url = ARGV[6] + WEBrick::HTTPUtils.escape(to) + '/' +
		#	media_url.split('/', 8)[7]
		proxy_url = 'ERROR - invalid'

		puts 'ORIG_URL: ' + media_url
		puts 'PROX_URL: ' + proxy_url

		# put URL in the body (so Conversations will still see it)...
		msg = Blather::Stanza::Message.new(to, proxy_url)
		msg.from = from
		msg.subject = subject if subject

		# ...but also provide URL in XEP-0066 (OOB) fashion
		# TODO: confirm client supports OOB or don't send this
		x = Nokogiri::XML::Node.new 'x', msg.document
		x['xmlns'] = 'jabber:x:oob'

		urln = Nokogiri::XML::Node.new 'url', msg.document
		urlc = Nokogiri::XML::Text.new proxy_url, msg.document
		urln.add_child(urlc)
		x.add_child(urln)

		if desc
			descn = Nokogiri::XML::Node.new('desc', msg.document)
			descc = Nokogiri::XML::Text.new(desc, msg.document)
			descn.add_child(descc)
			x.add_child(descn)
		end

		msg.add_child(x)

		write(msg)
	rescue Exception => e
		panic(e)
	end

	def self.error_msg(orig, query_node, type, name, text=nil)
		orig.type = :error

		error = Nokogiri::XML::Node.new 'error', orig.document
		error['type'] = type
		orig.add_child(error)

		suberr = Nokogiri::XML::Node.new name, orig.document
		suberr['xmlns'] = 'urn:ietf:params:xml:ns:xmpp-stanzas'
		error.add_child(suberr)

		orig.add_child(query_node) if query_node

		# TODO: add some explanatory xml:lang='en' text (see text param)
		puts "RESPONSE3: #{orig.inspect}"
		return orig
	end

	# workqueue_count MUST be 0 or else Blather uses threads!
	setup ARGV[0], ARGV[1], ARGV[2], ARGV[3], nil, nil, workqueue_count: 0

	def self.pass_on_message(m, users_num, jid)
		# setup delivery receipt; similar to a reply
		rcpt = ReceiptMessage.new(m.from.stripped)
		rcpt.from = m.to

		# pass original message (before sending receipt)
		m.to = jid
		m.from = "#{users_num}@#{ARGV[0]}"

		puts 'XRESPONSE0: ' + m.inspect
		write_to_stream m

		# send a delivery receipt back to the sender
		# TODO: send only when requested per XEP-0184
		# TODO: pass receipts from target if supported

		# TODO: put in member/instance variable
		rcpt['id'] = SecureRandom.uuid
		rcvd = Nokogiri::XML::Node.new 'received', rcpt.document
		rcvd['xmlns'] = 'urn:xmpp:receipts'
		rcvd['id'] = m.id
		rcpt.add_child(rcvd)

		puts 'XRESPONSE1: ' + rcpt.inspect
		write_to_stream rcpt
	end

	def self.call_vonagev0(
		token, secret, m, pth, body=nil,
		head={}, code=[200], respond_with=:body
	)
		EM::HttpRequest.new(
			"https://api.nexmo.com/#{pth}"
		).public_send(
			m,
			head: {
				'Authorization' => [token, secret]
			}.merge(head),
			body: body
		).then { |http|
			puts "API response to send: #{http.response} with code"\
				" response.code #{http.response_header.status}"

			if code.include?(http.response_header.status)
				case respond_with
				when :body
					http.response
				when :headers
					http.response_header
				else
					http
				end
			else
				EMPromise.reject(http.response_header.status)
			end
		}
	end

	def self.to_vonagev0_possible_oob(s, num_dest, user_id, token, secret,
		usern)

		REDIS.exists("blocked_sentinel-#{usern}").then { |is_blocked|
			if 1 == is_blocked
				t = Time.now
				puts "LOG %d.%09d: BLOCKED message for %s" %
					[t.to_i, t.nsec, usern]
				EMPromise.reject(
					[:modify, 'policy-violation']
				)
			end
		}.then {
			un = s.at("oob|x > oob|url", oob: "jabber:x:oob")
			if not un
				puts "MMSOOB: no url node found so process as normal"
				next to_vonagev0_possible_spam(s, num_dest, user_id,
					token, secret, usern)
			end
			puts "MMSOOB: found a url node - checking if to make MMS..."

			# TODO: check size of file at un.text and shrink if need

			body = s.respond_to?(:body) ? s.body : ''
			# some clients send URI in both body & <url/> so delete
			s.body = body.sub(/\s*#{Regexp::escape(un.text)}\s*$/, '')

			puts "MMSOOB: url text is '#{un.text}'"
			puts "MMSOOB: the body is '#{body.to_s.strip}'"

			# TODO: nil -> un.text (once MMS actually works right)
			puts "MMSOOB: sending MMS since found OOB & user asked"
			to_vonagev0(s, nil, num_dest, user_id, token,
				secret, usern)
		}
	end

	def self.to_vonagev0_possible_spam(s, num_dest, user_id, token, secret,
		usern)

		fine = false

		if s.body == @last_message_text[usern]
			@last_message_count[usern] += 1
			puts "dupe message count for #{usern} now at " +
				 @last_message_count[usern].to_s
		else
			@last_message_text[usern] = s.body
			@last_message_count[usern] = 1

			fine = true
		end

		if /http:\/\/|https:\/\// =~ s.body.to_s
			if @last_message_had_httpx[usern]
				@last_messages_with_httpx_count[usern] += 1
				puts "httpx message count for #{usern} now @ " +
				 @last_messages_with_httpx_count[usern].to_s
			end
			@last_message_had_httpx[usern] = true

			t = Time.now
			tai_timestamp = `./tai`.strip
			tai_yyyymmdd = Time.at(tai_timestamp.to_i).strftime(
				'%Y%m%d')
			REDIS.incr('usage_httpx-' + tai_yyyymmdd + '-' + usern)
		else
			@last_message_had_httpx[usern] = false
			@last_messages_with_httpx_count[usern] = 1

			if fine
				# return immediately to avoid unnecessary DB hit
				return to_vonagev0(s, nil, num_dest, user_id,
					token, secret, usern)
			end
		end

		REDIS.mget('settings_spam-dupe_min',
			'settings_spam-dupe_max').then { |(min, max)|

		# TODO: fix indenting
		if !min or !max
			min = 100
			max = 101
		end

		trigger_number = SecureRandom.random_number(min.to_i..max.to_i)

		if @last_message_count[usern] >= trigger_number

			REDIS.exists("settings_spam-allowlist-#{usern}").then { |allow|
				if 1 == allow

					t = Time.now
					puts "LOG %d.%09d: ALLOW avoids block for %s "\
						"with allow value %d and count %d "\
						"and trigger number %d (min/max %d/%d)"%
						[t.to_i, t.nsec, usern, allow,
							@last_message_count[usern],
							trigger_number, min, max]
					to_vonagev0(s, nil, num_dest, user_id, token,
						secret, usern)
				else
					REDIS.setnx("blocked_sentinel-#{usern}", '').then { |rv|

						t = Time.now
						puts "LOG %d.%09d: SPAM block added for %s "\
							"with rv %d, allow %d and count %d "\
							"and trigger number %d (min/max %d/%d)"%
							[t.to_i, t.nsec, usern, rv, allow,
								@last_message_count[usern],
								trigger_number, min, max]
						EMPromise.reject(
							[:modify, 'policy-violation']
						)
					}

				end
			}
		else
			REDIS.mget('settings_spam-subsequent_httpx_min',
				'settings_spam-subsequent_httpx_max').then { |(hx_min, hx_max)|

			# TODO: fix indenting
			if !hx_min or !hx_max
				hx_min = 100
				hx_max = 101
			end

			hx_trigger_number = SecureRandom.random_number(hx_min.to_i..hx_max.to_i)

			if @last_messages_with_httpx_count[usern] >= hx_trigger_number

				REDIS.exists("settings_spam-allowlist-#{usern}").then { |hallow|
					if 1 == hallow

						t = Time.now
						puts "LOG %d.%09d: ALLOW avoids h block for %s"\
							" with allow value %d and count %d "\
							"and trigger number %d (min/max %d/%d)"%
							[t.to_i, t.nsec, usern, hallow,
							 @last_messages_with_httpx_count[usern],
							 hx_trigger_number, hx_min, hx_max]
						to_vonagev0(s, nil, num_dest, user_id, token,
							secret, usern)
					else
						REDIS.setnx("blocked_sentinel-#{usern}",
							'').then { |hrv|

							t = Time.now
							puts "LOG %d.%09d: SPAM h block added "\
								"for %s with rv %d, allow %d "\
								"and count %d and trigger "\
								"number %d (min/max %d/%d)" %
							 [t.to_i, t.nsec, usern, hrv, hallow,
							 @last_messages_with_httpx_count[usern],
							 hx_trigger_number, hx_min, hx_max]
							EMPromise.reject(
								[:modify, 'policy-violation']
							)
						}

					end
				}
			else
				to_vonagev0(s, nil, num_dest, user_id, token,
					secret, usern)
			end
			}
		end
		}
	end

	def self.to_vonagev0(s, murl, num_dest, user_id, token, secret, usern)
		body = s.respond_to?(:body) ? s.body : ''
		if murl.to_s.empty? && body.to_s.strip.empty?
			return EMPromise.reject(
				[:modify, 'policy-violation']
			)
		end

		extra = if murl
			# TODO: won't work until murl put in sub-objects instead
			{
				media: murl
			}
		else
			{
			}
		end

		# callbacks need unbounded id, resourcepart; client_ref 40 chars
		callback_value = WEBrick::HTTPUtils.escape(s.id.to_s) + ' ' +
			WEBrick::HTTPUtils.escape(s.from.resource.to_s)
		ref_id = SecureRandom.uuid

		# Vonage delivery receipts (DLR) arrive in <72 hours so give 96h
		REDIS.setex("callback_value-dlr-#{ref_id}", 345600,
			callback_value).then { |rv|

			# TODO: actually check rv instead of just printing
			puts 'CBDLR setting return value: "' + rv.to_s + '"'

			call_vonagev0(
				token,
				secret,
				:post,
				'v0.1/messages',
				JSON.dump(extra.merge(
					to:	{
						type:	'sms',
						number:	num_dest[1..-1]
						},
					from:	{
						type:	'sms',
						number:	usern[1..-1]
						},
					message: { content: {
						type:	'text',
						text:	body
						}},
					client_ref:
						ref_id
				)),
				{'Content-Type' => 'application/json'},
				[202]
			)
		}.catch {
			# TODO: add text; mention code number
			EMPromise.reject(
				[:cancel, 'internal-server-error']
			)
		}

		t = Time.now
		tai_timestamp = `./tai`.strip
		tai_yyyymmdd = Time.at(tai_timestamp.to_i).strftime('%Y%m%d')
		puts "SMU %d.%09d, %s: msg for %s sent on %s - incrementing\n" %
			[t.to_i, t.nsec, tai_timestamp, usern, tai_yyyymmdd]

		REDIS.incr('usage_messages-' + tai_yyyymmdd + '-' +
			usern).then { |total|

			t = Time.now
			puts "SMU %d.%09d: total msgs for %s-%s now at %s\n" %
				[t.to_i, t.nsec, tai_yyyymmdd, usern, total]
		}
	end

	def self.validate_num(num)
		EMPromise.resolve(num.to_s).then { |num_dest|
			if num_dest =~ /\A\+?[0-9]+(?:;.*)?\Z/
				next num_dest if num_dest[0] == '+'
				shortcode = extract_shortcode(num_dest)
				next shortcode if shortcode
			end

			if is_anonymous_tel?(num_dest)
				EMPromise.reject([:cancel, 'gone'])
			else
				# TODO: text re num not (yet) supportd/implmentd
				EMPromise.reject([:cancel, 'item-not-found'])
			end
		}
	end

	def self.fetch_vonage_cred_for(jid)
		cred_key = "vonage_cred-#{jid.stripped}"
		REDIS.lrange(cred_key, 0, 3).then { |creds|
			if creds.length < 4
				# TODO: add text re credentials not registered
				EMPromise.reject(
					[:auth, 'registration-required']
				)
			else
				creds
			end
		}
	end

	message :error? do |m|
		# TODO: report it somewhere/somehow - eat for now so no err loop
		puts "EATERROR1: #{m.inspect}"
	end

	message :body do |m|
		EMPromise.all([
			validate_num(m.to.node),
			fetch_vonage_cred_for(m.from)
		]).then { |(num_dest, creds)|
			jid_key = "vonage_jid-#{num_dest}"
			REDIS.get(jid_key).then { |jid|
				[jid, num_dest] + creds
			}
		}.then { |(jid, num_dest, *creds)|
			if jid
				cred_key = "vonage_cred-#{jid}"
				REDIS.lrange(cred_key, 0, 0).then { |other_user|
					[jid, num_dest] + creds + other_user
				}
			else
				[jid, num_dest] + creds + [nil]
			end
		}.then { |(jid, num_dest, *creds, other_user)|
			# if destination user is in the system pass on directly
			if other_user and other_user.start_with? 'u-'
				pass_on_message(m, creds.last, jid)
			else
				to_vonagev0_possible_oob(m, num_dest, *creds)
			end
		}.catch { |e|
			if e.is_a?(Array) && e.length == 2
				write_to_stream error_msg(m.reply, m.body, *e)
			else
				EMPromise.reject(e)
			end
		}
	end

	def self.user_cap_identities
		[{category: 'client', type: 'sms'}]
	end

	def self.user_cap_features
		[
			"urn:xmpp:receipts",
		]
	end

	def self.add_gateway_feature(feature)
		@gateway_features << feature
		@gateway_features.uniq!
	end

	subscription :request? do |p|
		puts "PRESENCE1: #{p.inspect}"

		# subscriptions are allowed from anyone - send reply immediately
		msg = Blather::Stanza::Presence.new
		msg.to = p.from
		msg.from = p.to
		msg.type = :subscribed

		puts 'RESPONSE5a: ' + msg.inspect
		write_to_stream msg

		# send a <presence> immediately; not automatically probed for it
		# TODO: refactor so no "presence :probe? do |p|" duplicate below
		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://vonage.sgx.soprani.ca/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/sgx'

		puts 'RESPONSE5b: ' + msg.inspect
		write_to_stream msg

		# need to subscribe back so Conversations displays images inline
		msg = Blather::Stanza::Presence.new
		msg.to = p.from.to_s.split('/', 2)[0]
		msg.from = p.to.to_s.split('/', 2)[0]
		msg.type = :subscribe

		puts 'RESPONSE5c: ' + msg.inspect
		write_to_stream msg
	end

	presence :probe? do |p|
		puts 'PRESENCE2: ' + p.inspect

		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://vonage.sgx.soprani.ca/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/sgx'

		puts 'RESPONSE6: ' + msg.inspect
		write_to_stream msg
	end

	iq '/iq/ns:query', ns:	'http://jabber.org/protocol/disco#info' do |i|
		# TODO: return error if i.type is :set - if it is :reply or
		#  :error it should be ignored (as the below does currently);
		#  review specification to see how to handle other type values
		if i.type != :get
			puts 'DISCO iq rcvd, of non-get type "' + i.type.to_s +
				'" for message "' + i.inspect + '"; ignoring...'
			next
		end

		# respond to capabilities request for an sgx-vonagev0 number JID
		if i.to.node
			# TODO: confirm the node URL is expected using below
			#puts "XR[node]: #{xpath_result[0]['node']}"

			msg = i.reply
			msg.node = i.node
			msg.identities = user_cap_identities
			msg.features = user_cap_features

			puts 'RESPONSE7: ' + msg.inspect
			write_to_stream msg
			next
		end

		# respond to capabilities request for sgx-vonagev0 itself
		msg = i.reply
		msg.node = i.node
		msg.identities = [{
			name: 'Soprani.ca Gateway to XMPP - Vonage version 0.1',
			type: 'sms', category: 'gateway'
		}]
		msg.features = @gateway_features
		write_to_stream msg
	end

	def self.check_then_register(i, *creds)
		jid_key = "vonage_jid-#{creds.last}"
		bare_jid = i.from.stripped
		cred_key = "vonage_cred-#{bare_jid}"

		REDIS.get(jid_key).then { |existing_jid|
			if existing_jid && existing_jid != bare_jid
				# TODO: add/log text: credentials exist already
				EMPromise.reject([:cancel, 'conflict'])
			end
		}.then {
			REDIS.lrange(cred_key, 0, 3)
		}.then { |existing_creds|
			# TODO: add/log text: credentials exist already
			if existing_creds.length == 4 && creds != existing_creds
				EMPromise.reject([:cancel, 'conflict'])
			elsif existing_creds.length < 4
				REDIS.rpush(cred_key, *creds).then { |length|
					if length != 4
						EMPromise.reject([
							:cancel,
							'internal-server-error'
						])
					end
				}
			end
		}.then {
			# not necessary if existing_jid non-nil, easier this way
			REDIS.set(jid_key, bare_jid)
		}.then { |result|
			if result != 'OK'
				# TODO: add txt re push failure
				EMPromise.reject(
					[:cancel, 'internal-server-error']
				)
			end
		}.then {
			write_to_stream i.reply
		}
	end

	def self.creds_from_registration_query(qn)
		xn = qn.children.find { |v| v.element_name == "x" }

		if xn
			xn.children.each_with_object({}) do |field, h|
				next if field.element_name != "field"
				val = field.children.find { |v|
					v.element_name == "value"
				}

				case field['var']
				when 'username'
					h[:api_key] = val.text
				when 'password'
					h[:api_secret] = val.text
				when 'phone'
					h[:phone_num] = val.text
				else
					# TODO: error
					puts "?: #{field['var']}"
				end
			end
		else
			qn.children.each_with_object({}) do |field, h|
				case field.element_name
				when "username"
					h[:api_key] = field.text
				when "password"
					h[:api_secret] = field.text
				when "phone"
					h[:phone_num] = field.text
				end
			end
		end.values_at(:api_key, :api_secret, :phone_num)
	end

	def self.process_registration(i, qn)
		EMPromise.resolve(
			qn.children.find { |v| v.element_name == "remove" }
		).then { |rn|
			if rn
				puts "received <remove/> - ignoring for now..."
				EMPromise.reject(:done)
			else
				creds_from_registration_query(qn)
			end
		}.then { |api_key, api_secret, phone_num|
			if phone_num[0] == '+'
				[api_key, api_secret, phone_num]
			else
				# TODO: add text re number not (yet) supported
				EMPromise.reject([:cancel, 'item-not-found'])
			end
		}.then { |api_key, api_secret, phone_num|

			# we would use call_vonagev0() but Vonage is not
			#  consistent with its URIs and we need a non-standard
			#  prefix here (i.e. rest.nexmo.com instead of the usual
			#  api.nexmo.com) - should be only time so hard-code it
			EM::HttpRequest.new(
			  'https://rest.nexmo.com/account/numbers?api_key=' +
			  api_key + '&api_secret=' + api_secret
			).public_send(
				:get
			).then { |http|
				puts 'REG API response to send: "' +
					http.response.to_s + '" with code "' +
					http.response_header.status.to_s + '"'

				if 200 == http.response_header.status
					http.response
				else
					EMPromise.reject(
						http.response_header.status
					)
				end
			}.then { |response|
				params = JSON.parse(response)
				if params['numbers'].find { |number|
					number['msisdn'] == phone_num[1..-1] }

					check_then_register(
						i,
						'-',	# empty user, for compat
						api_key,
						api_secret,
						phone_num
					)
				else
					# TODO: add text re number not found
					EMPromise.reject([:modify,
						'not-acceptable'])
				end
			}
		}.catch { |e|
			EMPromise.reject(case e
			when 401
				# TODO: add text re bad credentials
				[:auth, 'not-authorized']
			when 404
				# TODO: add text re number not found or disabled
				[:cancel, 'item-not-found']
			when Integer
				[:modify, 'not-acceptable']
			else
				e
			end)
		}
	end

	def self.registration_form(orig, key=nil, existing_number=nil)
		msg = Nokogiri::XML::Node.new 'query', orig.document
		msg['xmlns'] = 'jabber:iq:register'

		if existing_number
			msg.add_child(
				Nokogiri::XML::Node.new(
					'registered', msg.document
				)
			)
		end

		n1 = Nokogiri::XML::Node.new(
			'instructions', msg.document
		)
		n1.content = "Enter the information from your Account "\
			"page as well as the Phone Number\nin your "\
			"account you want to use (ie. '+12345678901')"\
			".\nAPI Key is username, "\
			"API Secret is password, Phone Number is phone"\
			".\n\nThe source code for this gateway is at "\
			"https://gitlab.com/soprani.ca/sgx-vonagev0 ."\
			"\nCopyright (C) 2017-2020  Denver Gingerich "\
			"and others, licensed under AGPLv3+."
		n3 = Nokogiri::XML::Node.new 'username', msg.document
		n4 = Nokogiri::XML::Node.new 'password', msg.document
		n5 = Nokogiri::XML::Node.new 'phone', msg.document
		n5.content = existing_number.to_s
		msg.add_child(n1)
		msg.add_child(n3)
		msg.add_child(n4)
		msg.add_child(n5)

		x = Blather::Stanza::X.new :form, [
			{
				required: true, type: :"text-single",
				label: 'API Key', var: 'username',
				value: key.to_s
			},
			{
				required: true, type: :"text-private",
				label: 'API Secret', var: 'password'
			},
			{
				required: true, type: :"text-single",
				label: 'Phone Number', var: 'phone',
				value: existing_number.to_s
			}
		]
		x.title = 'Register for '\
			'Soprani.ca Gateway to XMPP - Vonage version 0.1'
		x.instructions = "Enter the details from your Account "\
			"page as well as the Phone Number\nin your "\
			"account you want to use (ie. '+12345678901')"\
			".\n\nThe source code for this gateway is at "\
			"https://gitlab.com/soprani.ca/sgx-vonagev0 ."\
			"\nCopyright (C) 2017-2020  Denver Gingerich "\
			"and others, licensed under AGPLv3+."
		msg.add_child(x)

		orig.add_child(msg)

		return orig
	end

	iq '/iq/ns:query', ns: 'jabber:iq:register' do |i, qn|
		puts "IQ: #{i.inspect}"

		case i.type
		when :set
			process_registration(i, qn)
		when :get
			bare_jid = i.from.stripped
			cred_key = "vonage_cred-#{bare_jid}"
			REDIS.lrange(cred_key, 1, 3).then { |key, sec, number|
				reply = registration_form(i.reply, key, number)
				puts "RESPONSE2: #{reply.inspect}"
				write_to_stream reply
			}
		else
			# Unknown IQ, ignore for now
			EMPromise.reject(:done)
		end.catch { |e|
			if e.is_a?(Array) && e.length == 2
				write_to_stream error_msg(i.reply, qn, *e)
			elsif e != :done
				EMPromise.reject(e)
			end
		}.catch(&method(:panic))
	end

	iq :get? do |i|
		write_to_stream error_msg(i.reply, i.children, 'cancel', 'feature-not-implemented')
	end

	iq :set? do |i|
		write_to_stream error_msg(i.reply, i.children, 'cancel', 'feature-not-implemented')
	end
end

class ReceiptMessage < Blather::Stanza
	def self.new(to=nil)
		node = super :message
		node.to = to
		node
	end
end

class WebhookHandler < Goliath::API
	use Goliath::Rack::Params

	def response(env)
		puts 'ENV: ' + env.reject{ |k| k == 'params' }.to_s

		if params.empty?
			puts 'PARAMS empty!'
			return [200, {}, "OK"]
		end

		if params['msisdn']
			users_num = '+' + params['to']
			others_num = params['msisdn']

			puts 'BODY - messageId: ' + params['messageId'] +
				', type: ' + params['type'] +
				', msgtimestamp: ' + params['message-timestamp']
		else
			users_num = '+' + params['from']['number']
			others_num = params['to']['number']

			puts 'BODY - message_uuid: ' + params['message_uuid'] +
				', to[type]: ' + params['to']['type'] +
				', from[type]: ' + params['from']['type'] +
				', timestamp: ' + params['timestamp'] +
				', usage[price]: ' + (params['usage'] ?
					params['usage']['price'] : 'NONE') +
				', usage[currency]: ' + (params['usage'] ?
					params['usage']['currency'] : 'NONE') +
				', error[code]: ' + (params['error'] ?
					params['error']['code'].to_s : 'NONE') +
				', error[reason]: ' + (params['error'] ?
					params['error']['reason'] : 'NONE') +
				', status: ' + params['status'] +
				', client_ref: ' + params['client_ref']
		end

		# This cannot differentiate between short code and alphanumeric sender
		# Since we don't support sending to short codes, and have no
		# defined phone-context for this case, at least this allows delivery
		if others_num =~ /\A\d+\Z/ && others_num.length > 6
			others_num = "+#{others_num}"
		else
			others_num = escape_jid(others_num)
		end

		jid_key = "vonage_jid-#{users_num}"
		bare_jid = REDIS.get(jid_key).promise.sync

		if !bare_jid
			puts "jid_key (#{jid_key}) DNE; Vonage misconfigured?"

			# TODO: likely not appropriate; give error to Vonage?
			# TODO: add text re credentials not being registered
			#write_to_stream error_msg(m.reply, m.body, :auth,
			#	'registration-required')
			return [200, {}, "OK"]
		end

		msg = ''
		if params['msisdn']
			text = ''
			case params['type']
			when 'text'
				text = params['text']
			# TODO: figure out what this will be (to handle MMS)
			when 'mms'
				has_media = false
				params['media'].each do |media_url|
					if not media_url.end_with?(
						'.smil', '.txt', '.xml'
					)

						has_media = true
						SGXvonagev0.send_media(
							others_num + '@' +
							ARGV[0],
							bare_jid, media_url
						)
					end
				end

				if params['text'].empty?
					if not has_media
						text = '[suspected group msg '\
							'with no text (odd)]'
					end
				else
					text = if has_media
						# TODO: write/use a caption XEP
						params['text']
					else
						'[suspected group msg '\
						'(recipient list not '\
						'available) with '\
						'following text] ' +
						params['text']
					end
				end

				# ie. if text param non-empty or had no media
				if not text.empty?
					msg = Blather::Stanza::Message.new(
						bare_jid, text)
					msg.from = others_num + '@' + ARGV[0]
					SGXvonagev0.write(msg)
				end

				return [200, {}, "OK"]
			else
				text = "unknown type (#{params['type']})"\
					" with text: " + params['text']

				# TODO: log/notify of this properly
				puts text
			end

			msg = Blather::Stanza::Message.new(bare_jid, text)
		else

			cb_val_key =
				"callback_value-dlr-#{params['client_ref']}"
			callback_value = REDIS.get(cb_val_key).promise.sync

			# TODO: confirm callback_value non-empty, etc.

			puts "CBDLR value '#{callback_value}'"

			tag_parts = callback_value.split(/ /, 2)
			id = WEBrick::HTTPUtils.unescape(tag_parts[0])
			resourcepart = WEBrick::HTTPUtils.unescape(tag_parts[1])

			puts "CBDLR id '#{id}' resourcepart '#{resourcepart}'"

			case params['status']
			when 'rejected'
				# create a bare message like the one user sent
				msg = Blather::Stanza::Message.new(
					others_num + '@' + ARGV[0])
				msg.from = bare_jid + '/' + resourcepart
				msg['id'] = id

				# create an error reply to the bare message
				msg = Blather::StanzaError.new(
					msg,
					'recipient-unavailable',
					:wait
				).to_node
			when 'undeliverable'
				# create a bare message like the one user sent
				msg = Blather::Stanza::Message.new(
					others_num + '@' + ARGV[0])
				msg.from = bare_jid + '/' + resourcepart
				msg['id'] = id

				# create an error reply to the bare message
				msg = Blather::StanzaError.new(
					msg,
					'item-not-found',
					:cancel
				).to_node
			when 'delivered'
				msg = ReceiptMessage.new(bare_jid)

				# TODO: put in member/instance variable
				msg['id'] = SecureRandom.uuid

				# TODO: send only when requested per XEP-0184
				rcvd = Nokogiri::XML::Node.new(
					'received',
					msg.document
				)
				rcvd['xmlns'] = 'urn:xmpp:receipts'
				rcvd['id'] = id
				msg.add_child(rcvd)
			when 'submitted'
				# can't really do anything with it; nice to know
				puts "message with id #{id} submitted"
				return [200, {}, "OK"]
			else
				# TODO: notify somehow of unknown state receivd?
				puts "message with id #{id} has "\
					"other state #{params['status']}"
				return [200, {}, "OK"]
			end

			puts "RESPONSE4: #{msg.inspect}"
		end

		msg.from = others_num + '@' + ARGV[0]
		SGXvonagev0.write(msg)

		[200, {}, "OK"]

	rescue Exception => e
		puts 'Shutting down gateway due to exception 013: ' + e.message
		SGXvonagev0.shutdown
		puts 'Gateway has terminated.'
		EM.stop
	end
end

at_exit do
	$stdout.sync = true

	puts "Soprani.ca/SMS Gateway for XMPP - Vonage version 0.1\n"\
		"==>> last commit of this version is " + `git rev-parse HEAD` + "\n"

	if ARGV.size != 5
		puts "Usage: sgx-vonagev0.rb <xmpp_component_jid> "\
			"<xmpp_component_password> <xmpp_server_hostname> "\
			"<xmpp_server_port> <http_listen_port> "
		exit 0
	end

	t = Time.now
	puts "LOG %d.%09d: starting...\n\n" % [t.to_i, t.nsec]

	EM.run do
		REDIS = EM::Hiredis.connect

		SGXvonagev0.run

		# required when using Prosody otherwise disconnects on 6-hour inactivity
		EM.add_periodic_timer(3600) do
			msg = Blather::Stanza::Iq::Ping.new(:get, 'localhost')
			msg.from = ARGV[0]
			SGXvonagev0.write(msg)
		end

		server = Goliath::Server.new('0.0.0.0', ARGV[4].to_i)
		server.api = WebhookHandler.new
		server.app = Goliath::Rack::Builder.build(server.api.class, server.api)
		server.logger = Log4r::Logger.new('goliath')
		server.logger.add(Log4r::StdoutOutputter.new('console'))
		server.logger.level = Log4r::INFO
		server.start do
			["INT", "TERM"].each do |sig|
				trap(sig) do
					EM.defer do
						puts 'Shutting down gateway...'
						SGXvonagev0.shutdown

						puts 'Gateway has terminated.'
						EM.stop
					end
				end
			end
		end
	end
end
